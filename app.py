import time
from typing import List
from flask import Flask, request, jsonify
from colorthief import ColorThief
from urllib.request import urlopen
from diffusers import StableDiffusionPipeline
import torch
import io

app = Flask(__name__)


@app.route("/")
def main():
    return "<p>Welcome to our theme ai helper </p>"


@app.route('/palette-extract')
def palette_extract():
    url = request.args.get('url', default="", type=str)
    if not url:
        return jsonify({'success': False, 'message': "Validation Error", 'data': None})

    try:
        fd = urlopen(url)
        f = io.BytesIO(fd.read())

        color_thief = ColorThief(f)
        colors = color_thief.get_palette(color_count=2, quality=1)
        palette = []
        for color in colors:
            (r, g, b) = color
            palette.append("#{:02x}{:02x}{:02x}".format(r, g, b))

        return jsonify({'success': True, "message": "Done successfully", 'data': palette})

    except:
        return jsonify({'success': False, 'message': "Error parsing your image", 'data': None})


@app.route('/generate-images')
def generate_images():
    prompts = request.args.getlist('prompts')
    paths = []
    model_id = "dreamlike-art/dreamlike-photoreal-2.0"
    pipeline = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float32)
    # try:
    for i, prompt in enumerate(prompts):
        image = pipeline(prompt).images[0]
        path = f"temps/temp_{time.time()}_{i}.jpg"
        image.save(path)
        paths.append(path)
    return jsonify({'success': True, "message": "Done successfully", 'data': paths})

    # except:
    #     return jsonify({'success': False, 'message': "Error parsing your image", 'data': None})
